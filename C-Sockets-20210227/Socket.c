/*

* Funciones de lectura y escritura en sockets
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>



/*
* Lee datos del socket. 
* Regresa el número de bytes leidos o
* 0 si se cierra el archivo  o 
* -1 si hay error
*/
int Lee_Socket (int fd, char *Datos, int Longitud)
{
	int Leido = 0;
	int Aux = 0;

	/*
	* Comprobacion de los parametros de entrada 
	*/
	if ((fd == -1) || (Datos == NULL) || (Longitud < 1))
		return -1;

	/*
	* Leer todos los datos
	*/
	while (Leido < Longitud)
	{
		Aux = read (fd, Datos + Leido, Longitud - Leido);
		if (Aux > 0)
		{
			/* 
			* si se leen todos los datos, se incrementa la variable
			* que contiene los datos leidos hasta el momento
			*/
			Leido = Leido + Aux;
		}
		else
		{
			/*
			* Si read devuelve 0 (se creó el socket). 
			* Regresan los caracteres leidos 
			*/
			if (Aux == 0) 
				return Leido;
			if (Aux == -1)
			{
				/*
				* En caso de existir, la variable errno indica el tipo de error. 
				* EINTR se produce si hay interrupcion del sistema 
				* antes de lectura
				* EGAIN socket no disponible
				* Ambos errores se tratan con una espera de 100 microsegundos
				* y se vuelve a intentar.
				* Se sale de la función con cualquier otro error.
				*/
				switch (errno)
				{
					case EINTR:
					case EAGAIN:
						usleep (100);
						break;
					default:
						return -1;
				}
			}
		}
	}

	/*
	regresa el total de caracteres leidos
	*/
	return Leido;
}

/*
* Escribe un dato en el socket cliente. Devuelve numero de bytes escritos,
* o -1 si hay error.
*/
int Escribe_Socket (int fd, char *Datos, int Longitud)
{
	int Escrito = 0;
	int Aux = 0;

	/*
	* Comprobacion de los parametros de entrada
	*/
	if ((fd == -1) || (Datos == NULL) || (Longitud < 1))
		return -1;

	
	while (Escrito < Longitud)
	{
		Aux = write (fd, Datos + Escrito, Longitud - Escrito);
		if (Aux > 0)
		{
			Escrito = Escrito + Aux;
		}
		else
		{
			/*
			* Si se ha cerrado el socket: numero de caracteres
			* leidos.
			* Si ha habido error: -1
			*/
			if (Aux == 0)
				return Escrito;
			else
				return -1;
		}
	}

	/*
	* Regresa el total de caracteres leidos
	*/
	return Escrito;
}
