
#include <stdio.h>
#include <Socket_Cliente.h>
#include <Socket.h>

main ()
{
	/*
	* Descriptor del socket y buffer de datos
	*/
	int Socket_Con_Servidor;
	char Cadena[100];

	/*
	* Abrir conexion con el servidor, nombre del servidory el servicio.
	*
	* "localhost": dado de alta en /etc/hosts
	* "prueba_s":  dado de alta en /etc/services
	*/
	Socket_Con_Servidor = Abre_Conexion_Inet ("localhost", "prueba_s");
	if (Socket_Con_Servidor == 1)
	{
		printf ("No se pudo establecer conexion con el servidor\n");
		exit (-1);
	}

	/*
	* Se prepara una cadena con 5 caracteres y se envia
	*/

	strcpy (Cadena, "Hola");
	Escribe_Socket (Socket_Con_Servidor, Cadena, 5);

	/*
	* Se lee la informacion enviada por el servidor (cadena de 6 caracteres).
	*/
	Lee_Socket (Socket_Con_Servidor, Cadena, 6);

	printf ("Proceso cliente, recibido : %s\n", Cadena);

	/*
	* Se cierra el socket 
	*/
	close (Socket_Con_Servidor);
}
